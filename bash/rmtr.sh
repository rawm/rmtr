#Author :Ray Mintz
#email: raymondmintz@edgecast.com 
#TODO:
#	Optimize the testPops() method.
#	Add function to check for packet loss @ final destination 
##########################################################################

#!/bin/bash

fileDir=/tmp/
mainFile=$fileDir/$USER.mainFile.txt
ARGNUM=$#
argList=(${@})
sources=${argList[0]}
packetCount=${argList[1]}
destinations=(${argList[@]:2})
popsPass=True
jsaNum=1
counter=0

checkArgs(){
	
	if [ $ARGNUM -lt 3 ]; then
		echo "Usage: ./rmtr {main pop} packetCount {other pops}"
		exit 0
	fi
	
}

testPops(){
	jsaServer="jsa00$jsaNum."
	
	while [ ${#destinations[@]} -ne 0 ] 
		do
			for apc in ${destinations[@]}
				do
				  ping -c 1 $jsaServer$apc > /dev/null
				  result=$?
				  if [ $result == 0 ]; then
					unset destinations[$counter]
					((counter++))
					command="\n\nmtr $sources $jsaServer$apc -c $packetCount"
					echo -e $command > $HOME/mtr-$apc.txt
					mtr --report --report-cycle $packetCount $jsaServer$apc >> $HOME/mtr-$apc.txt & 
					jsaNum=1
					testPops
				  else
					  ((jsaNum++))
					  if [ $jsaNum -gt 4 ] ; then
						  echo -e "no JSA for $apc\nreplace and try again"
						  exit 1
					  fi
					testPops
				  fi
				done
		done
}  

clearFiles(){
	rm $HOME/mtr*

	if [ -f $HOME/mainMTR.txt ]; then
	  rm $HOME/mainMTR.txt
	fi
}

makeMainFile(){
	
	for mtrFile in $(ls $HOME/mtr*)
	  do
	    cat $mtrFile >> $HOME/mainMTR.txt
	  done
	
	echo "Main file complete"
	
	
}

main(){
  
  echo $sources
  echo $destinations
  checkArgs
  clearFiles
  testPops
  echo "All Pops pass ping test"
  echo "Performing mtr's and Writing to $HOME/mainMTR.txt"
  waitTimer=$((packetCount+10))
  sleep $waitTimer
  makeMainFile
  cat $HOME/mainMTR.txt
  
}

main
